﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Xml;

namespace Updater.Models
{
    internal class UpdateChecker
    {
        private const string DOWNLOAD_LINK = "https://dasanko.com/download.php?f=";
        private const string PROGRAMS_XML = "https://dasanko.com/dasanko/programs.xml";

        public Tuple<Version, string> GetData(string title)
        {
            WebClient updater = new WebClient();

            updater.Headers["Accept"] = "application/xml";
            updater.Headers["User-Agent"] = title;

            Version onlineVersion = new Version();
            string changelog = string.Empty;

            try
            {
                using (Stream stream = updater.OpenRead(PROGRAMS_XML))
                using (XmlReader reader = XmlReader.Create(stream))
                {
                    bool stopReading = false;

                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Text:
                                if (reader.Value == title) // If <Name> is the application's name...
                                {
                                    for (int i = 0; i < 4; i++)
                                        reader.Read();
                                    onlineVersion = Version.Parse(reader.Value);

                                    for (int i = 0; i < 4; i++)
                                        reader.Read();
                                    changelog = reader.Value;

                                    stopReading = true;
                                }
                                break;
                        }

                        if (stopReading)
                            break;
                    }
                }
            }
            catch (Exception)
            {
                Debug.WriteLine("Error checking the update information.");  // The update checker should never present any crashes to the user
            }

            return new Tuple<Version, string>(onlineVersion, changelog);
        }

        public void PerformUpdate(string title)
        {
            string programUpdateFile = title + ".zip";

            WebClient updater = new WebClient();

            updater.Headers["Accept"] = "application/xml";
            updater.Headers["User-Agent"] = title;

            Logger("update process started");

            try
            {
                updater.DownloadFile(DOWNLOAD_LINK + programUpdateFile, programUpdateFile);

                string programName = string.Empty;

                using (Process parentProcess = ParentProcessUtils.ParentProcessUtilities.GetParentProcess())
                {
                    programName = parentProcess.ProcessName + ".exe";
                    parentProcess.Kill();
                }

                Logger(programName + " killed");

                Stream check = null;

                while (check == null)
                {
                    try
                    {
                        using (check = new FileStream(programName, FileMode.Open)) { }
                    }
                    catch (IOException) { }
                }

                File.Delete(programName);

                Logger(programName + " deleted");

                using (ZipArchive zipFile = ZipFile.OpenRead(programUpdateFile))
                    foreach (var file in zipFile.Entries)
                    {
                        if (file.FullName.EndsWith("/"))
                            Directory.CreateDirectory(file.FullName.Remove(file.FullName.Length - 1));
                        else if (file.FullName != Process.GetCurrentProcess().MainModule.ModuleName)
                            file.ExtractToFile(file.FullName, true);
                    }

                Logger(programUpdateFile + " extracted");

                check = null;

                while (check == null)
                {
                    try
                    {
                        using (check = new FileStream(programUpdateFile, FileMode.Open)) { }
                    }
                    catch (IOException) { }
                }

                File.Delete(programUpdateFile);

                Logger(programUpdateFile + " deleted");

                Process.Start(programName);

                Environment.Exit(0);
            }
            catch (Exception e) { Debug.WriteLine("Error while performing the update."); Logger(e.Message); }
        }

        [Conditional("DEBUG")]
        private void Logger(string toWrite)
        {
            File.AppendAllText("updater-debug.log", toWrite + Environment.NewLine);
        }
    }
}
