﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using Updater.Models;

namespace Updater
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class UpdateWindow : Window
    {
        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        private string title;

        public UpdateWindow(string title, string curVersionStr, string assemblyName)
        {
            InitializeComponent();
            Title = title; // Window's title
            this.title = title; // Global variable set to local

            Version curVersion = new Version(curVersionStr);

            UpdateChecker updater = new UpdateChecker();
            var updateData = updater.GetData(title);
            Version onlineVersion = updateData.Item1;

            curVerLbl.Content = curVersion;
            onlineVerLbl.Content = onlineVersion;

            if (onlineVersion > curVersion)
            {
                curVerLbl.Foreground = Brushes.Red;
                onlineVerLbl.Foreground = Brushes.Green;

                Title += " - Update found!";

                updateBtn.IsEnabled = true;
            }
            else
            {
                curVerLbl.Foreground = Brushes.Black;
                onlineVerLbl.Foreground = Brushes.Black;

                Title += " - No new updates available";

                updateBtn.IsEnabled = false;
            }

            textBox.Text = updateData.Item2;
        }

        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private void CloseCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void updateBtn_Click(object sender, RoutedEventArgs e)
        {
            UpdateChecker updater = new UpdateChecker();
            updater.PerformUpdate(title);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var hwnd = new WindowInteropHelper(this).Handle;
            SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
        }
    }
}
