﻿using System;
using System.Windows;

namespace Updater
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            try
            {
                if (e.Args.Length == 3)
                    new UpdateWindow(e.Args[0], e.Args[1], e.Args[2]).Show();
                else
                    Environment.Exit(1);
            }
            catch (Exception) { Environment.Exit(2); }
        }
    }
}
